﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace AglTest.Common
{
    /// <summary>
    /// Common methods
    /// </summary>

    public static class Common
    {
        /// <summary>
        /// This is the common method for getting json data from the api
        /// </summary>
        /// <returns></returns>
        public static string getJSONData()
        {
            try
            {
                WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["JSONUrl"].ToString());
                request.Method = "GET";
                if (ConfigurationManager.AppSettings["AuthorizationRequired"].ToString() == "Y")
                {
                    string credentials = ConfigurationManager.AppSettings["AuthorizationUsername"].ToString() + ":" + ConfigurationManager.AppSettings["AuthorizationPassword"].ToString();
                    request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(credentials));
                    ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                }
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string serverResponse = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return serverResponse;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to accept all certifications
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="certification"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        public static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}