﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CatsByGender.aspx.cs" Inherits="AglTest.CatsByGender" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="main.css" rel="stylesheet" />
    <script>
        $(function () {
            $("#accordion").accordion();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="btnSort" runat="server" Text="SortByDescending" OnClick="btnSort_Click" />
            <br />
            <asp:Literal ID="ltlDivCatsByGender" runat="server"></asp:Literal>
        </div>
        
    </form>
</body>
</html>
