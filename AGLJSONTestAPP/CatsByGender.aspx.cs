﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using static AglTest.Common.Common;
using AglTest.BAL;
using static AglTest.BO.BO;
using AglTest.Common;
using AglTest.BO;

namespace AglTest
{
    public partial class CatsByGender : BaseClass
    {
        #region Page Evenets
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack) {
                    DisplayCatsFromPeopleAPI(Constants.SortType.SortByAscending.ToString());
                }
            }
            catch (Exception)
            {
                //Log in database
            }
        }
        #endregion

        #region Private methods  
        /// <summary>
        /// This is the main method to display cats from people api.
        /// </summary>
        /// <param name="sortOrder">Sory Order</param>
        private void DisplayCatsFromPeopleAPI(string sortOrder)
        {
            #region For People
            var jObjectP = new JObject();
            dynamic jsonP = JValue.Parse(getJSONData());
            List<People> lstPeople = jsonP.ToObject<List<People>>();
            DisplayCats(lstPeople, sortOrder);
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstPeople"></param>
        /// <param name="sortOrder"></param>
        private void DisplayCats(List<People> lstPeople, string sortOrder)
        {
            AglTestBAL objBAL = new AglTestBAL();
            ltlDivCatsByGender.Text = objBAL.BuildCatsByGender(lstPeople, sortOrder).ToString();
        }
        #endregion

        #region Events

        /// <summary>
        /// Sort Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSort_Click(object sender, EventArgs e)
        {
            if(btnSort.Text == Constants.SortType.SortByAscending.ToString())
            {
                btnSort.Text = Constants.SortType.SortByDescending.ToString();
                DisplayCatsFromPeopleAPI(Constants.SortType.SortByAscending.ToString());
            }
            else if (btnSort.Text == Constants.SortType.SortByDescending.ToString())
            {
                btnSort.Text = Constants.SortType.SortByAscending.ToString();
                DisplayCatsFromPeopleAPI(Constants.SortType.SortByDescending.ToString());
            }
        }
        #endregion  
    }
}