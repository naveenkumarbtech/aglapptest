﻿using AglTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using static AglTest.BO.BO;

namespace AglTest.BAL
{
    public class AglTestBAL :BaseClass
    {
        /// <summary>
        /// This method is used to build the api on ui page
        /// </summary>
        /// <param name="lstPeople"></param>
        public StringBuilder BuildCatsByGender(List<People> lstPeople, string sortOrder)
        {
            List<string> lstGender = lstPeople.Select(x => x.Gender).Distinct().ToList();
            StringBuilder strCats = new StringBuilder();
            strCats = strCats.Append("<div id=divPets'>");
            foreach (var itemGenderName in lstGender)
            {
                strCats = strCats.Append("<h3> " + itemGenderName + " </h3>");
                strCats = strCats.Append("<div>");
                strCats = strCats.Append("<ul>");
                lstPeople = RemovePeopleWithNoPets(lstPeople);
                List<People> lst = lstPeople.Where(a => a.Gender == itemGenderName).ToList();
                List<Pets> lstPet = lst.SelectMany(q => q.Pets).Where(a => a.Type == "Cat").ToList();
                if (sortOrder == Constants.SortType.SortByAscending.ToString())
                {
                    lstPet = lstPet.OrderBy(x => x.Name).ToList();
                }
                else
                {
                    lstPet = lstPet.OrderByDescending(x => x.Name).ToList();
                }
                foreach (var itemPet in lstPet)
                {
                    strCats = strCats.Append("<li>" + itemPet.Name + "</li>");
                }
                strCats = strCats.Append("</ul>");
            }
            strCats = strCats.Append("</div>");
            return strCats;
        }

        /// <summary>
        /// This method remove people object if there is no pets.
        /// </summary>
        /// <param name="lstPeople">list of people</param>
        /// <returns></returns>
        public List<People> RemovePeopleWithNoPets(List<People> lstPeople)
        {
            foreach (var innerlist in lstPeople.ToList())
            {
                if ((innerlist.Pets) == null)
                {
                    lstPeople.Remove(innerlist);
                }
            }
            return lstPeople;
        }

    }
}