﻿using System.Collections.Generic;

namespace AglTest.BO
{
    /// <summary>
    /// Bussiness Objects
    /// </summary>
    public class BO
    {
        public class People
        {
            public string Name { get; set; }
            public string Gender { get; set; }
            public int Age { get; set; }
            public List<Pets> Pets { get; set; }
        }
        public class Pets
        {
            public string Name { get; set; }
            public string Type { get; set; }
        }
    }
}